.PHONY: quick slow clean docs debug benchmark

quick:
	cmake -S . -B ./build && cd ./build && make -j && cd ..

slow:
	cmake -S . -B ./build && cd ./build && make && cd ..

benchmark: quick
	@printf '\n\e[1;34m Starting benchmark... \e[0m \n\n'
	@./bin/benchmark
	@printf '\n\e[1;34m End of benchmark \e[0m \n\n'


clean:
	rm -rf ./bin/[a-zA-Z0-9]* ./build/[a-zA-Z0-9]*

docs: 
	@doxygen ./doc/image.doxy
	@mkdir -p /tmp/doc-project
	@firefox --private-window ./doc/html/index.html

debug: quick 
	@printf '\n\e[1;34m Executing GDB on the binaries... \e[0m \n\n'
	gdb -ex run ./bin/test -ex quit || gdb -ex start ./bin/test
	@printf '\n\e[1;34m Executing valgrind on the binaries... \e[0m \n\n'
	valgrind --tool=memcheck --suppressions=./doc/valgrind_lif7.supp -s --leak-check=full ./bin/test
