#ifndef _BST_DOD
#define _BST_DOD

#include "bst-base.hpp"
#include "tree-node.hpp"

#include <iostream>

// DECLARATIONS

//! @brief This class is a smart wrapper of the class TreeNode.
template <typename TreeType> class BST_DOD : public BST_BASE<TreeType *> {
protected:
  // the valueTab of value
  TreeType *valueTab;

  // inside function :
  void createValueTab();

  // NOTE : this function should mimic the behavior of the
  // TreeNode::insertNode() maybe make this protected
  void insertNode(TreeNode<TreeType *> *newNode,
                  TreeNode<TreeType *> *fromNode);

public:
  // CONSTRUCTORS
  BST_DOD();
  // BST_DOD(TreeNode<TreeType> *newRoot);

  // this can be used to create a BST_DOD on any given tab
  BST_DOD(TreeType *newTab, const unsigned int newTabSize,
          const unsigned int newMaxTabSize = 0);

  // DESTRUCTOR
  ~BST_DOD();

  // GETTERS
  TreeType *get_valueTab() const;

  // SETTERS
  void set_valueTab(TreeType *newTab);

  // OTHERS

  void createTabs();
  void dropTabs();

  // the methods below should be protected
  // or maybe not?
  void dropValueTab();
  void dropNodeTab();

  // valueTab related
  void sortTab(); // this need a lot of work

  // node related

  // NOTE : this function use BST_DOD::inserNode() method
  void addNode(const TreeType &valueToAdd);
  void removeNode(TreeNode<TreeType *> *nodeToRemove);

  //! @brief The right branch take the place of the root of the subtree, and the
  //! old root is inserted from the new root of this subtree. In other words,
  //! every node moves one position to left from the current position.
  void rotationLL(TreeNode<TreeType *> *node);

  //! @brief The left branch take the place of the root of the subtree, and the
  //! old root is inserted from the new root of this subtree. In other words,
  //! every node moves one position to right from the current position.
  void rotationRR(TreeNode<TreeType *> *node);

  //! @brief We perform rotationLL then rotationRR.
  void rotationLR(TreeNode<TreeType *> *node);

  //! @brief We perform rotationRR then rotationLL
  void rotationRL(TreeNode<TreeType *> *node);

  //! @brief This method tidy the tree to be balanced.
  //! @note Does this function call isBalanced() before doing anything?
  void tidyTree();

  void rebuildTree(); // this 'allocate' the node with the value contained in
                      // this->valueTab (from 0 to this->treeSize)
};

// DEFINITIONS

template <typename TreeType>
BST_DOD<TreeType>::BST_DOD() : BST_BASE<TreeType *>() {
  this->valueTab = nullptr;
}

template <typename TreeType>
BST_DOD<TreeType>::BST_DOD(TreeType *newValueTab, const unsigned int newTabSize,
                           const unsigned int newMaxTabSize)
    : BST_BASE<TreeType *>(newTabSize, newMaxTabSize) {
  this->set_valueTab(newValueTab);
}

template <typename TreeType> BST_DOD<TreeType>::~BST_DOD() {
  // this method should use the dropTabs methods
  if (this->get_valueTab() != nullptr) {
    delete[] this->valueTab;
    this->set_valueTab(nullptr);
  }
  if (this->nodeTab != nullptr) {
    delete[] this->nodeTab;
    this->nodeTab = nullptr;
    this->set_treeRoot(nullptr);
  }
}

template <typename TreeType> TreeType *BST_DOD<TreeType>::get_valueTab() const {
  return this->valueTab;
}

template <typename TreeType>
void BST_DOD<TreeType>::set_valueTab(TreeType *newTab) {
  // NOTE : do this function should delete the old table?
  this->valueTab = newTab;
}

template <typename TreeType> void BST_DOD<TreeType>::createValueTab() {
  this->valueTab = new TreeType[this->maxTreeSize];
}

template <typename TreeType> void BST_DOD<TreeType>::createTabs() {
  this->createValueTab();
  this->createNodeTab();
}

template <typename TreeType>
void BST_DOD<TreeType>::insertNode(TreeNode<TreeType *> *newNode,
                                   TreeNode<TreeType *> *fromNode) {
  // check if we don't insert a node on itself
  if (fromNode != newNode) {
    // check the which node point toward the greater value
    if (*newNode->value < *fromNode->value) {
      if (fromNode->get_branch_left() == nullptr) {
        fromNode->set_branch_left(newNode);
      } else {
        this->insertNode(newNode, fromNode->get_branch_left());
      }
    } else {
      if (fromNode->get_branch_right() == nullptr) {
        fromNode->set_branch_right(newNode);
      } else {
        this->insertNode(newNode, fromNode->get_branch_right());
      }
    }
  }
}

template <typename TreeType>
void BST_DOD<TreeType>::addNode(const TreeType &valueToAdd) {
  if (this->get_treeRoot() == nullptr) {
    this->createNodeTab();
    this->set_treeRoot(&this->nodeTab[0]);
  }
  if (this->get_valueTab() == nullptr) {
    this->createValueTab();
  }

  // give the node the proper value
  this->valueTab[this->treeSize] = valueToAdd;
  this->nodeTab[this->treeSize].value = &this->valueTab[this->treeSize];

  // insert the node in the tree
  this->insertNode(&this->nodeTab[this->treeSize], this->get_treeRoot());

  this->treeSize++; // increment the size of the tree

  // if the treeTab becomes too small
  if (this->treeSize >= (this->maxTreeSize - 1)) {
    // NOTE : when trees get a layer deeper, they grow differently than this
    // formula.
    // TODO : use the correct formula
    this->maxTreeSize *= 2;
    // create a new valueTab with the correct size
    TreeType *newTab = new TreeType[this->maxTreeSize];
    TreeNode<TreeType *> *newNodeTab =
        new TreeNode<TreeType *>[this->maxTreeSize];

    // copy the value in the new valueTab
    for (unsigned int i = 0; i < this->treeSize; i++) {
      // copy the value
      newTab[i] = this->valueTab[i];

      // set the value of the node to be the adress where to find the value
      newNodeTab[i].value = &newTab[i];
    }

    // NOTE : those instruction below should be in their own methods?
    // TODO : fix this.
    delete[] this->valueTab;
    delete[] this->nodeTab;

    // NOTE : the expression below should be a method?
    this->nodeTab = newNodeTab;
    // NOTE : this is not okay, the root of the tree shouldn't be anything, but
    // the node at the same index of the root in the old table.
    // TODO : fix this.
    this->set_treeRoot(this->nodeTab);

    this->set_valueTab(newTab);

    // this->tidyTree(); // this need to be implemented

    // need to recreate all the node with the correct link
  }
}

template <typename TreeType>
void BST_DOD<TreeType>::removeNode(TreeNode<TreeType *> *nodeToRemove) {}

template <typename TreeType>
void BST_DOD<TreeType>::rotationLL(TreeNode<TreeType *> *node) {
  // if the node provided is nullptr we do nothing
  if (node != nullptr && !this->isNodeBalanced(node)) {
    // we create placeholder
    TreeNode<TreeType *> *parentOfNode = node->get_parent();
    TreeNode<TreeType *> *rightBranchOfNode = node->get_branch_right();

    if (rightBranchOfNode != nullptr) {

      // we disconnect the node from its right branch
      node->set_branch_right(nullptr);
      rightBranchOfNode->set_parent(nullptr);

      // then insert the new root of this subtree to the node's parent
      if (parentOfNode == nullptr) {
        // if the node is the root, we just need to update the tree with the new
        // root
        this->set_treeRoot(rightBranchOfNode);
      } else {
        // we replace the link from the parent to the new branch, which is the
        // node's right branch
        if (node->isNodeLeftBranchOfParent()) {
          parentOfNode->set_branch_left(rightBranchOfNode);
        }
        if (node->isNodeRightBranchOfParent()) {
          parentOfNode->set_branch_right(rightBranchOfNode);
        }
        // we disconnect the node to its parent
        node->set_parent(nullptr);
      }
      // then insert the node at its old right branch
      insertNode(node, rightBranchOfNode);
    }
  }
}

template <typename TreeType>
void BST_DOD<TreeType>::rotationRR(TreeNode<TreeType *> *node) {}

template <typename TreeType>
void BST_DOD<TreeType>::rotationLR(TreeNode<TreeType *> *node) {}

template <typename TreeType>
void BST_DOD<TreeType>::rotationRL(TreeNode<TreeType *> *node) {}

template <typename TreeType> void BST_DOD<TreeType>::tidyTree() {}

#endif