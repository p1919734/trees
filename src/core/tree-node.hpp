#ifndef _TREE_NODE
#define _TREE_NODE

#include <algorithm> // this is only usefull for std::max

// DECLARATIONS

//! @brief This class is designed to be used with some sort of smart wrapper
//! class, or inherited to add functionnalities.
template <typename NodeType> class TreeNode {
protected:
  //! @brief Pointer on the parent TreeNode.
  TreeNode *parent;
  // if == nullptr then its the root
  // OR IS IT? *vsauce theme start*

  //! @brief Pointer of the left son TreeNode.
  TreeNode *branch_left;

  //! @brief Pointer of the right son TreeNode.
  TreeNode *branch_right;

public:
  // DATA MEMBERS
  //! @brief The data member containing the value of the TreeNode.
  NodeType value; // this doesn't need any protection

  // CONSTRUCTORS
  TreeNode(NodeType setupValue, TreeNode *setupBranch_left = nullptr,
           TreeNode *setupBranch_right = nullptr);
  TreeNode(TreeNode *setupBranch_left = nullptr,
           TreeNode *setupBranch_right = nullptr);
  // DESTRUCTORS
  // NOTE :
  //   ———————————No destructors? ———————
  // ⠀⣞⢽⢪⢣⢣⢣⢫⡺⡵⣝⡮⣗⢷⢽⢽⢽⣮⡷⡽⣜⣜⢮⢺⣜⢷⢽⢝⡽⣝
  // ⠸⡸⠜⠕⠕⠁⢁⢇⢏⢽⢺⣪⡳⡝⣎⣏⢯⢞⡿⣟⣷⣳⢯⡷⣽⢽⢯⣳⣫⠇
  // ⠀⠀⢀⢀⢄⢬⢪⡪⡎⣆⡈⠚⠜⠕⠇⠗⠝⢕⢯⢫⣞⣯⣿⣻⡽⣏⢗⣗⠏⠀
  // ⠀⠪⡪⡪⣪⢪⢺⢸⢢⢓⢆⢤⢀⠀⠀⠀⠀⠈⢊⢞⡾⣿⡯⣏⢮⠷⠁⠀⠀
  // ⠀⠀⠀⠈⠊⠆⡃⠕⢕⢇⢇⢇⢇⢇⢏⢎⢎⢆⢄⠀⢑⣽⣿⢝⠲⠉⠀⠀⠀⠀
  // ⠀⠀⠀⠀⠀⡿⠂⠠⠀⡇⢇⠕⢈⣀⠀⠁⠡⠣⡣⡫⣂⣿⠯⢪⠰⠂⠀⠀⠀⠀
  // ⠀⠀⠀⠀⡦⡙⡂⢀⢤⢣⠣⡈⣾⡃⠠⠄⠀⡄⢱⣌⣶⢏⢊⠂⠀⠀⠀⠀⠀⠀
  // ⠀⠀⠀⠀⢝⡲⣜⡮⡏⢎⢌⢂⠙⠢⠐⢀⢘⢵⣽⣿⡿⠁⠁⠀⠀⠀⠀⠀⠀⠀
  // ⠀⠀⠀⠀⠨⣺⡺⡕⡕⡱⡑⡆⡕⡅⡕⡜⡼⢽⡻⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  // ⠀⠀⠀⠀⣼⣳⣫⣾⣵⣗⡵⡱⡡⢣⢑⢕⢜⢕⡝⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  // ⠀⠀⠀⣴⣿⣾⣿⣿⣿⡿⡽⡑⢌⠪⡢⡣⣣⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  // ⠀⠀⠀⡟⡾⣿⢿⢿⢵⣽⣾⣼⣘⢸⢸⣞⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  // ⠀⠀⠀⠀⠁⠇⠡⠩⡫⢿⣝⡻⡮⣒⢽⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
  // ————————————————————————————————————

  // GETTERS
  TreeNode *get_parent() const;
  TreeNode *get_branch_left() const;
  TreeNode *get_branch_right() const;

  TreeNode *get_root() const;

  // SETTERS
  void set_parent(TreeNode *newParent);
  void set_branch_left(TreeNode *newBranch_left);
  void set_branch_right(TreeNode *newBranch_right);

  // OTHERS

  // tools
  bool isNodeLeftBranchOfParent() const;
  bool isNodeRightBranchOfParent() const;

  bool isLeaf() const;
  int calculateNodeDepth() const;
  int distanceFromLeaf() const;

  void insertNode(TreeNode *newNode);
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// DEFINITIONS

template <typename NodeType>
TreeNode<NodeType>::TreeNode(NodeType setupValue, TreeNode *setupBranch_left,
                             TreeNode *setupBranch_right) {
  this->value = setupValue;
  this->parent = nullptr;
  this->set_branch_left(setupBranch_left);
  this->set_branch_right(setupBranch_right);
}

template <typename NodeType>
TreeNode<NodeType>::TreeNode(TreeNode *setupBranch_left,
                             TreeNode *setupBranch_right) {
  this->parent = nullptr;
  this->set_branch_left(setupBranch_left);
  this->set_branch_right(setupBranch_right);
}

template <typename NodeType>
TreeNode<NodeType> *TreeNode<NodeType>::get_parent() const {
  return this->parent;
}

template <typename NodeType>
TreeNode<NodeType> *TreeNode<NodeType>::get_branch_left() const {
  return this->branch_left;
}

template <typename NodeType>
TreeNode<NodeType> *TreeNode<NodeType>::get_branch_right() const {
  return this->branch_right;
}

template <typename NodeType>
TreeNode<NodeType> *TreeNode<NodeType>::get_root() const {
  TreeNode *buf = this->ptr;
  while (buf->get_parent() != nullptr) {
    buf = buf->get_parent();
  }
  return buf;
}

template <typename NodeType>
void TreeNode<NodeType>::set_parent(TreeNode *newParent) {
  this->parent = newParent;
}

template <typename NodeType>
void TreeNode<NodeType>::set_branch_left(TreeNode *newBranch_left) {
  this->branch_left = newBranch_left;
  if (this->get_branch_left() != nullptr) {
    this->branch_left->parent = this;
  }
}

template <typename NodeType>
void TreeNode<NodeType>::set_branch_right(TreeNode *newBranch_right) {
  this->branch_right = newBranch_right;
  if (this->get_branch_right() != nullptr) {
    this->branch_right->parent = this;
  }
}

template <typename NodeType>
bool TreeNode<NodeType>::isNodeLeftBranchOfParent() const {
  if (this->get_parent() == nullptr) {
    return false;
  } else {
    return (this->get_parent()->get_branch_left() == this);
  }
}

template <typename NodeType>
bool TreeNode<NodeType>::isNodeRightBranchOfParent() const {
  if (this->get_parent() == nullptr) {
    return false;
  } else {
    return (this->get_parent()->get_branch_right() == this);
  }
}

template <typename NodeType> bool TreeNode<NodeType>::isLeaf() const {
  return ((this->get_branch_left() == nullptr) &&
          (this->get_branch_right() == nullptr));
}

template <typename NodeType>
int TreeNode<NodeType>::calculateNodeDepth() const {
  if (this->get_parent() != nullptr) { // test if this isn't the root
    TreeNode *buf = this->get_parent();
    int cnt = 1;
    while (buf->get_parent() != nullptr) {
      buf = buf->get_parent();
      cnt++;
    }
    return cnt;
  } else {    // if this is the root
    return 0; // the root is at depth 0
  }
}

template <typename NodeType> int TreeNode<NodeType>::distanceFromLeaf() const {
  if (this->isLeaf()) { // we test if the node is a leaf, if it is return 0
    return 0;           // the distance from the leaf if you're the leaf is 0
  } else {
    // if only one of the branch is empty, then the other is the one leading to
    // a leaf
    unsigned int depth_left = 0;
    if (this->get_branch_left() != nullptr) {
      depth_left = this->get_branch_left()->distanceFromLeaf();
    }

    unsigned int depth_right = 0;
    if (this->get_branch_right() != nullptr) {
      depth_left = this->get_branch_right()->distanceFromLeaf();
    }

    return std::max(depth_left, depth_right) + 1;
  }
}

template <typename NodeType>
void TreeNode<NodeType>::insertNode(TreeNode *newNode) {
  if (this != newNode) {
    if (newNode->value < this->value) {
      if (this->get_branch_left() == nullptr) {
        this->set_branch_left(newNode);
      } else {
        this->get_branch_left()->insertNode(newNode);
      }
    } else {
      if (this->get_branch_right() == nullptr) {
        this->set_branch_right(newNode);
      } else {
        this->get_branch_right()->insertNode(newNode);
      }
    }
  }
}

#endif
