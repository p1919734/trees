#ifndef _BST_OOP
#define _BST_OOP

#include <functional>
#include <iostream>

#include "bst-base.hpp"
#include "tree-node.hpp"

// DECLARATIONS

//! @brief This class is designed to use TreeNode in a 'manual' way.
//! @note this should be refactored into a class that contain TreeNode but
//! don't allocate memory in a continuous way
template <typename TreeType> class BST_OOP : public BST_BASE<TreeType> {
protected:
public:
  BST_OOP();

  BST_OOP(const unsigned int newTabSize, const unsigned int newMaxTabSize = 0);

  ~BST_OOP();

  void addNode(const TreeType &value);

  //! @brief The right branch take the place of the root of the subtree, and the
  //! old root is inserted from the new root of this subtree. In other words,
  //! every node moves one position to left from the current position.
  void rotationLL(TreeNode<TreeType> *node);

  //! @brief The left branch take the place of the root of the subtree, and the
  //! old root is inserted from the new root of this subtree. In other words,
  //! every node moves one position to right from the current position.
  void rotationRR(TreeNode<TreeType> *node);

  //! @brief We perform rotationLL then rotationRR.
  void rotationLR(TreeNode<TreeType> *node);

  //! @brief We perform rotationRR then rotationLL
  void rotationRL(TreeNode<TreeType> *node);

  //! @brief This method tidy the tree to be balanced.
  //! @note Does this function call isBalanced() before doing anything?
  void tidyTree();

  //! @brief This method drop every branch below recursively.
  // void dropTree();
};

// DEFINITIONS
template <typename TreeType>
BST_OOP<TreeType>::BST_OOP() : BST_BASE<TreeType>() {}

template <typename TreeType>
BST_OOP<TreeType>::BST_OOP(const unsigned int newTabSize,
                           const unsigned int newMaxTabSize)
    : BST_BASE<TreeType>(newTabSize, newMaxTabSize) {}

template <typename TreeType> BST_OOP<TreeType>::~BST_OOP() {}

template <typename TreeType>
void BST_OOP<TreeType>::addNode(const TreeType &value) {
  if (this->get_treeRoot() == nullptr) {
    this->createNodeTab();
    this->set_treeRoot(&this->nodeTab[0]);
  }

  // give the node the proper value
  this->nodeTab[this->treeSize].value = value;

  // insert the node in the tree
  this->get_treeRoot()->insertNode(&this->nodeTab[this->treeSize]);

  this->treeSize++; // increment the size of the tree

  // if the treeTab becomes too small
  if (this->treeSize >= (this->maxTreeSize - 1)) {
    // NOTE : when trees get a layer deeper, they grow differently than this
    // formula.
    // TODO : use the correct formula
    this->maxTreeSize *= 2;

    TreeNode<TreeType> *newNodeTab = new TreeNode<TreeType>[this->maxTreeSize];

    // copy the value in the new valueTab
    for (unsigned int i = 0; i < this->treeSize; i++) {
      // set the value of the node to be the adress where to find the value
      newNodeTab[i].value = this->nodeTab[i].value;
    }

    // NOTE : those instruction below should be in their own methods?
    // TODO : fix this.
    delete[] this->nodeTab;

    // NOTE : the expression below should be a method?
    this->nodeTab = newNodeTab;
    // NOTE : this is not okay, the root of the tree shouldn't be anything, but
    // the node at the same index of the root in the old table.
    // TODO : fix this.
    this->set_treeRoot(this->nodeTab);

    // this->tidyTree(); // this need to be implemented

    // need to recreate all the node with the correct link
  }
}

template <typename TreeType>
void BST_OOP<TreeType>::rotationLL(TreeNode<TreeType> *node) {
  // if the node provided is nullptr we do nothing
  if (node != nullptr && !this->isNodeBalanced(node)) {
    // we create placeholder
    TreeNode<TreeType> *parentOfNode = node->get_parent();
    TreeNode<TreeType> *rightBranchOfNode = node->get_branch_right();

    if (rightBranchOfNode != nullptr) {

      // we disconnect the node from its right branch
      node->set_branch_right(nullptr);
      rightBranchOfNode->set_parent(nullptr);

      // then insert the new root of this subtree to the node's parent
      if (parentOfNode == nullptr) {
        // if the node is the root, we just need to update the tree with the new
        // root
        this->set_treeRoot(rightBranchOfNode);
      } else {
        // we replace the link from the parent to the new branch, which is the
        // node's right branch
        if (node->isNodeLeftBranchOfParent()) {
          parentOfNode->set_branch_left(rightBranchOfNode);
        }
        if (node->isNodeRightBranchOfParent()) {
          parentOfNode->set_branch_right(rightBranchOfNode);
        }
        // we disconnect the node to its parent
        node->set_parent(nullptr);
      }
      // then insert the node at its old right branch
      rightBranchOfNode->insertNode(node);
    }
  }
}

template <typename TreeType>
void BST_OOP<TreeType>::rotationRR(TreeNode<TreeType> *node) {}

template <typename TreeType>
void BST_OOP<TreeType>::rotationLR(TreeNode<TreeType> *node) {}

template <typename TreeType>
void BST_OOP<TreeType>::rotationRL(TreeNode<TreeType> *node) {}

template <typename TreeType> void BST_OOP<TreeType>::tidyTree() {}

// NOTE : This function shouldn't be here but instead a lambda in the
// dropTree() function!
// template <typename TreeType> void dropBranches(TreeNode<TreeType> *node) {

//   // std::cout << "Node's adress is : " << node << std::endl;
//   // std::cout << "Node's value is : " << node->value << std::endl;

//   if (node->get_branch_left() != nullptr) {
//     // enter recursion
//     dropBranches(node->get_branch_left());

//     // after the recursion, we empty the node correctly
//     delete node->get_branch_left();
//     node->set_branch_left(nullptr);
//   }

//   if (node->get_branch_right() != nullptr) {
//     // enter recursion
//     dropBranches(node->get_branch_right());

//     // after the recursion, we empty the node correctly
//     delete node->get_branch_right();
//     node->set_branch_right(nullptr);
//   }
// }

// WARNING : THIS FUNCTION MAY CAUSE SEVERE MEMORY LEAK. USE WITH EXTREME
// CARE.
// template <typename TreeType> void BST_OOP<TreeType>::dropTree() {
//   if (!this->isEmpty()) {
//     // declaring this function here so it can't be used anywhere else
//     // std::function<TreeNode<TreeType> *> dropBranches =
//     //     [&](TreeNode<TreeType> *node) {
//     //       // std::cout << "Node's adress is : " << node << std::endl;
//     //       // std::cout << "Node's value (adress) is : " << node->value
//     //       //           << std::endl;
//     //       // std::cout << "Node's value (dereferenced) is : " <<
//     *node->value
//     //       //           << std::endl;

//     //       if (node->get_branch_left() != nullptr) {
//     //         // enter recursion
//     //         dropBranches(node->get_branch_left());

//     //         // after the recursion, we empty the node correctly
//     //         delete node->get_branch_left();
//     //         node->set_branch_left(nullptr);
//     //       }

//     //       if (node->get_branch_right() != nullptr) {
//     //         // enter recursion
//     //         dropBranches(node->get_branch_right());

//     //         // after the recursion, we empty the node correctly
//     //         delete node->get_branch_right();
//     //         node->set_branch_right(nullptr);
//     //       }
//     //     };

//     // we use the function declared before to empty to sub tree
//     dropBranches(this->treeRoot);
//     if (this->get_treeRoot() != nullptr) {
//       delete this->treeRoot;
//       this->set_treeRoot(nullptr);
//     }
//   }
// }
#endif
