#ifndef _BST_BASE
#define _BST_BASE

#include "tree-node.hpp"

// DECLARATIONS

//! @warning THIS CLASS ISN'T INTENDED TO BE USED AS SUCH, BUT RATHER
//! INTHERITED BY OTHER. For exemple like the classes BST_DOD and BST_OOP
//! @brief This class is a some kind of wrapper for the class TreeNode.
//! @note Does the static methods need to be 'protected' ?
template <typename TreeType> class BST_BASE {
protected:
  // ALL THE NODE'S VALUE ARE POINTER TOWARD THE TEMPLATED TYPE
  TreeNode<TreeType> *treeRoot;

  TreeNode<TreeType> *nodeTab;

  unsigned int treeSize;
  unsigned int maxTreeSize;
  // the maxTreeSize assume that the tree is balanced, so it
  // should follow some binary value : 1,3,7,15,31,63 and so on

  // Each time the trees grows (nodeTab and valueTab are reallocated) all the
  // adresses will change, this means that a lot of compute power is needed when
  // this happens, maybe using std::vector is the solution

  void createNodeTab();

  //! @brief This method check if a node as a balanced subtree.
  static bool isNodeBalanced(TreeNode<TreeType> *root);

public:
  // CONSTRUCTORS
  BST_BASE();
  BST_BASE(const unsigned int newTabSize, const unsigned int newMaxTabSize = 0);
  // BST_BASE(TreeNode<TreeType> *newRoot);

  // DESTRUCTOR
  ~BST_BASE();

  // GETTERS
  TreeNode<TreeType> *get_treeRoot() const;

  // SETTERS
  void set_treeRoot(TreeNode<TreeType> *newRoot); // make this protected

  // OTHERS
  //! @brief This method check if the tree is empty.
  bool isEmpty() const;

  // tree related

  //! @brief This method return the balance factor of a given node.
  //! @note Maybe this method should be protected.
  static int balanceFactor(TreeNode<TreeType> *node);

  //! @brief This method check if the tree is balanced.
  bool isBalanced() const;

  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  // ITERATORS

  // Classes for iterator for the differents way you can go through a tree
  // NOTE : read this
  // https://internalpointers.com/post/writing-custom-iterators-modern-cpp
  class template_iterator {
  protected:
    TreeNode<TreeType> &m_ptr;

  public:
    // NOTE : doesn't all iterator begin at the tree root?
    template_iterator begin();

    TreeNode<TreeType> *operator*();

    // NOTE : passing par reference, maybe extract the pointer?
    // NOTE : need to made function friends to be better
    bool operator==(const template_iterator &equal);
    bool operator!=(const template_iterator &notEqual);
  };
  /////////////////////////////////////////////////////////////////////////////
  class iterator_prefix : public template_iterator {
  protected:
  public:
    iterator_prefix operator++();
    // bool operator==(const iterator_prefix &equal);
    TreeType operator*();
    iterator_prefix begin();
    iterator_prefix end();
  };
  /////////////////////////////////////////////////////////////////////////////
  class iterator_infix : public template_iterator {
  protected:
  public:
    iterator_infix operator++();
    // bool operator==(const iterator_infix &equal);
    TreeType operator*();
    iterator_infix begin();
    // iterator_infix end();
  };
  /////////////////////////////////////////////////////////////////////////////
  class iterator_postfix : public template_iterator {
  protected:
  public:
    iterator_postfix operator++();
    // bool operator==(const iterator_postfix &equal);
    TreeType operator*();
    iterator_postfix begin();
    // iterator_postfix end();
  };
};

// DEFINITIONS

template <typename TreeType> BST_BASE<TreeType>::BST_BASE() {
  this->treeRoot = nullptr;
  this->nodeTab = nullptr;
  this->treeSize = 0;
  this->maxTreeSize = 1;
}

template <typename TreeType>
BST_BASE<TreeType>::BST_BASE(const unsigned int newTabSize,
                             const unsigned int newMaxTabSize) {
  this->treeSize = newTabSize;

  // if the value is set correctly (aka greater than 0)
  // NOTE : this doesn't check if newMaxTabSize is greater than newTabSize,
  // this could cause a bug.
  // TODO : fix this.
  if (newMaxTabSize <= 0) {
    // set with the value provided
    this->maxTreeSize = newMaxTabSize;
  } else {
    // by default, make the maxTreeSize twice as big as the treeSize
    this->maxTreeSize = 2 * this->treeSize;
  }
  this->createNodeTab();
}

template <typename TreeType> BST_BASE<TreeType>::~BST_BASE() {
  if (this->nodeTab != nullptr) {
    delete[] this->nodeTab;
    this->nodeTab = nullptr;
    this->set_treeRoot(nullptr);
  }
}

template <typename TreeType>
TreeNode<TreeType> *BST_BASE<TreeType>::get_treeRoot() const {
  return this->treeRoot;
}

template <typename TreeType>
void BST_BASE<TreeType>::set_treeRoot(TreeNode<TreeType> *newRoot) {
  this->treeRoot = newRoot;
}

template <typename TreeType> void BST_BASE<TreeType>::createNodeTab() {
  this->nodeTab = new TreeNode<TreeType>[this->maxTreeSize];
}

template <typename TreeType> bool BST_BASE<TreeType>::isEmpty() const {
  return (this->get_treeRoot() == nullptr);
}

template <typename TreeType>
bool BST_BASE<TreeType>::isNodeBalanced(TreeNode<TreeType> *node) {
  // if the node doesn't exist or if it's a leaf, then it's balanced
  // similar to the way than 0 is even
  if (node == nullptr || node->isLeaf()) {
    return true;
  } else {
    if (abs(balanceFactor(node)) <= 1 &&
        isNodeBalanced(node->get_branch_left()) &&
        isNodeBalanced(node->get_branch_right())) {
      return true;
    } else {
      return false;
    }
  }
}

template <typename TreeType>
int BST_BASE<TreeType>::balanceFactor(TreeNode<TreeType> *node) {
  int left_branch_height = 0;
  int right_branch_height = 0;

  // test if the branches do exist before calling distanceFromLeaf(),
  // otherwise it goes SEGFAULT
  if (node->get_branch_left() != nullptr) {
    left_branch_height = node->get_branch_left()->distanceFromLeaf();
  }
  if (node->get_branch_right() != nullptr) {
    right_branch_height = node->get_branch_right()->distanceFromLeaf();
  }

  return (left_branch_height - right_branch_height);
}

template <typename TreeType> bool BST_BASE<TreeType>::isBalanced() const {
  return this->isNodeBalanced(this->get_treeRoot());
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// ITERATORS

template <typename TreeType>
typename BST_BASE<TreeType>::template_iterator
BST_BASE<TreeType>::template_iterator::begin() {
  return this->ptr->get_root();
}

template <typename TreeType>
TreeNode<TreeType> *BST_BASE<TreeType>::template_iterator::operator*() {
  return this->ptr;
}

template <typename TreeType>
bool BST_BASE<TreeType>::template_iterator::operator==(
    const template_iterator &equal) {
  return this->m_ptr == equal.m_ptr;
}

template <typename TreeType>
bool BST_BASE<TreeType>::template_iterator::operator!=(
    const template_iterator &notEqual) {
  return !(this == notEqual);
}
///////////////////////////////////////////////////////////////////////////////
// PREFIX
template <typename TreeType>
typename BST_BASE<TreeType>::iterator_prefix
BST_BASE<TreeType>::iterator_prefix::begin() {
  return this->m_ptr; // is it okay?
}

template <typename TreeType>
typename BST_BASE<TreeType>::iterator_prefix
BST_BASE<TreeType>::iterator_prefix::end() {
  BST_BASE *buf = this->ptr;
  while (buf->get_branch_right() != nullptr) {
    buf = buf->get_branch_right();
  }
  return buf; // maybe return buf+1 ? since end() shouldn't return an accessible
              // adress
}

template <typename TreeType>
typename BST_BASE<TreeType>::iterator_prefix
BST_BASE<TreeType>::iterator_prefix::operator++() {
  // TODO : add case of tree root == nullptr
  // TODO : add end case with end(), what to do when it's the last node?
  if (this->ptr->get_branch_left() == nullptr) {
    if (this->ptr->get_branch_right() == nullptr) {
      // buffer that travel from parent to great-parent
      BST_BASE *buf = nullptr;
      do {
        buf = this->ptr->get_parent();
      } while (buf->get_branch_right() != nullptr &&
               this->ptr == buf->get_branch_right());
      this->ptr = buf->get_branch_right();
    } else {
      this->ptr = this->ptr->get_branch_right();
    }
  } else {
    this->ptr = this->ptr->get_branch_left();
  }
}

// template <typename TreeType>
// bool BST_BASE<TreeType>::iterator_prefix::operator==(
//     const iterator_prefix &equal) {
//   return this == equal;
// }

template <typename TreeType>
TreeType BST_BASE<TreeType>::iterator_prefix::operator*() {}

///////////////////////////////////////////////////////////////////////////////
// INFIX
template <typename TreeType>
typename BST_BASE<TreeType>::iterator_infix
BST_BASE<TreeType>::iterator_infix::operator++() {}

// template <typename TreeType>
// bool BST_BASE<TreeType>::iterator_infix::operator==(
//     const iterator_infix &equal) {
//   return this == equal;
// }

template <typename TreeType>
TreeType BST_BASE<TreeType>::iterator_infix::operator*() {}

///////////////////////////////////////////////////////////////////////////////
// POSTFIX
template <typename TreeType>
typename BST_BASE<TreeType>::iterator_postfix
BST_BASE<TreeType>::iterator_postfix::operator++() {}

// template <typename TreeType>
// bool BST_BASE<TreeType>::iterator_postfix::operator==(
//     const iterator_postfix &equal) {
//   return this == equal;
// }

template <typename TreeType>
TreeType BST_BASE<TreeType>::iterator_postfix::operator*() {}

#endif