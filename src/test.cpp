#include "core/bst-base.hpp"
#include "core/bst-dod.hpp"
#include "core/bst-oop.hpp"
#include "core/kd-tree.hpp"
#include "core/tree-node.hpp"

#include <cassert>
#include <chrono>
#include <iostream>

int test_TreeNode() {
  auto start = std::chrono::high_resolution_clock::now();

  TreeNode<int> test1;
  test1.value = 1;
  assert(test1.value == 1);

  TreeNode<int> *test2 = new TreeNode<int>;
  test2->value = 2;
  assert(test2->value == 2);

  // test2 is inserted and should be the right branch of test1 since it's
  // bigger.
  test1.insertNode(test2);

  assert(test1.get_branch_right() == test2);

  // we verify that test2 is really the right branch of test1
  assert(!test2->isNodeLeftBranchOfParent());
  assert(test2->isNodeRightBranchOfParent());

  test1.set_branch_right(nullptr);
  delete test2;

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  return duration.count();
}

int test_BST_BASE() {
  auto start = std::chrono::high_resolution_clock::now();

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  return duration.count();
}

// small recursion function
void BST_OOPRecursionWithRotationLL(BST_OOP<int> &tree, TreeNode<int> *node) {
  if (node->get_branch_right() != nullptr) {
    BST_OOPRecursionWithRotationLL(tree, node->get_branch_right());
  }
  tree.rotationLL(node);
}

int test_BST_OOP() {
  auto start = std::chrono::high_resolution_clock::now();

  const int SIZEOFTREE = 10;

  BST_OOP<int> test;

  // the tree is empty, so it is balanced
  assert(test.isBalanced());

  for (int i = 0; i < SIZEOFTREE; i++) {
    test.addNode(i);
  }
  // the tree is not balanced as all element are in the root right branch
  assert(!test.isBalanced());

  BST_OOPRecursionWithRotationLL(test, test.get_treeRoot());
  assert(test.isBalanced());

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  return duration.count();
}

// small recursion function
void BST_DODRecursionWithRotationLL(BST_DOD<int> &tree, TreeNode<int *> *node) {
  if (node->get_branch_right() != nullptr) {
    BST_DODRecursionWithRotationLL(tree, node->get_branch_right());
  }
  tree.rotationLL(node);
}

int test_BST_DOD() {
  auto start = std::chrono::high_resolution_clock::now();

  const int SIZEOFTREE = 10;

  BST_DOD<int> test;
  // the tree is empty, so it is balanced
  assert(test.isBalanced());
  for (int i = 0; i < SIZEOFTREE; i++) {
    test.addNode(i);
  }
  // the tree is not balanced as all element are in the root right branch
  assert(!test.isBalanced());

  BST_DODRecursionWithRotationLL(test, test.get_treeRoot());
  // assert(test.isBalanced());

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  return duration.count();
}

void test_kdtrees() {}

int main(void) {
  auto start = std::chrono::high_resolution_clock::now();

  // CODE HERE
  test_TreeNode();
  test_BST_BASE();
  test_BST_OOP();
  test_BST_DOD();
  // CODE HERE

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  int count = duration.count();
  std::cout << "Total execution time in µs : " << count << std::endl;

  return 0;
}