#include "core/bst-base.hpp"
#include "core/bst-dod.hpp"
#include "core/bst-oop.hpp"
#include "core/kd-tree.hpp"
#include "core/tree-node.hpp"

#include <chrono>
#include <fstream>
#include <iostream>
#include <math.h>

const int MAXSAMPLESIZE_POW2 = 15;
const int NUMBER_OF_PASS_ON_SAMPLE = 10;

template <typename NodeType>
void dropTreeNodeBranches(TreeNode<NodeType> *node) {
  if (node->get_branch_left() != nullptr) {
    // enter recursion
    dropTreeNodeBranches(node->get_branch_left());

    // after the recursion, we empty the node correctly
    delete node->get_branch_left();
    node->set_branch_left(nullptr);
  }

  if (node->get_branch_right() != nullptr) {
    // enter recursion
    dropTreeNodeBranches(node->get_branch_right());

    // after the recursion, we empty the node correctly
    delete node->get_branch_right();
    node->set_branch_right(nullptr);
  }
}

int test_allocation_node(const int sampleSize) {
  auto start = std::chrono::high_resolution_clock::now();

  TreeNode<int> *root = new TreeNode<int>;
  root->value = 0;
  for (int i = 0; i < sampleSize; i++) {
    TreeNode<int> *buf = new TreeNode<int>;
    buf->value = i + 1;
    root->insertNode(buf);
  }

  dropTreeNodeBranches(root);
  delete root;

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  return duration.count();
}

int test_allocation_BST_OOP(const int sampleSize) {
  auto start = std::chrono::high_resolution_clock::now();

  BST_OOP<int> tree;

  for (int i = 0; i < sampleSize; i++) {
    tree.addNode(i);
  }

  // need to create the iterator to check de value

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  return duration.count();
}

int test_allocation_BST_DOD(const int sampleSize) {
  auto start = std::chrono::high_resolution_clock::now();

  BST_DOD<int> tree;

  for (int i = 0; i < sampleSize; i++) {
    tree.addNode(i);
  }

  // need to create the iterator to check de value

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  return duration.count();
}

void test_allocation_kdtrees() {}

void test_allocation_all() {
  std::string filename = "data/benchmark_allocation.csv";

  std::ofstream fichier(filename.c_str());
  if (fichier.is_open()) {

    fichier << NUMBER_OF_PASS_ON_SAMPLE << " pass by sample's size"
            << ", Average exec time (in µs) for TreeNode"
            << ", for BST_OOP"
            << ", for BST_DOD" << '\n';

    for (int i = 0; i <= MAXSAMPLESIZE_POW2; i++) {
      // NOTE : use the Kibi/Gibi format instead of power of ten!
      double sampleSizeForTest = pow(2, i);

      double averageTreeNode = 0;
      double averageBST_OOP = 0;
      double averageBST_DOD = 0;

      for (int j = 0; j < NUMBER_OF_PASS_ON_SAMPLE; j++) {
        averageTreeNode += test_allocation_node(sampleSizeForTest);
        averageBST_OOP += test_allocation_BST_OOP(sampleSizeForTest);
        averageBST_DOD += test_allocation_BST_DOD(sampleSizeForTest);
      }

      averageTreeNode /= NUMBER_OF_PASS_ON_SAMPLE;
      averageBST_OOP /= NUMBER_OF_PASS_ON_SAMPLE;
      averageBST_DOD /= NUMBER_OF_PASS_ON_SAMPLE;
      fichier << sampleSizeForTest << ", " << averageTreeNode << ", "
              << averageBST_OOP << ", " << averageBST_DOD << '\n';
    }

    fichier.close();

  } else {
    std::cerr << "ERROR : CANNOT OPEN FILE" << std::endl;
  }
}

int main(void) {
  auto start = std::chrono::high_resolution_clock::now();

  test_allocation_all();

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  double count_in_seconds = duration.count() / 1000000.0;
  std::cout << "Total execution time in s : " << count_in_seconds << std::endl;

  return 0;
}